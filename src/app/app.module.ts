import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContainerComponent } from './cover/container/container.component';
import { CustomMapComponent } from './cover/custom-map/custom-map.component';

import { CustomOneshotComponent } from './cover/custom-oneshot/custom-oneshot.component';
import { CustomContinuousComponent } from './cover/custom-continuous/custom-continuous.component';

import { ChildDiagnosticMessageComponent } from './cover/child-diagnostic-message/child-diagnostic-message.component';

import { GoogleApiKeyService } from './cover/services/google-api-key.service';
import { GeoobservableService } from './cover/services/geoobservable.service';

@NgModule({
  declarations: [
    AppComponent
    , ContainerComponent
    , CustomMapComponent
    , CustomOneshotComponent
    , CustomContinuousComponent
    , ChildDiagnosticMessageComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    GoogleApiKeyService
    , GeoobservableService
  ] , 
  bootstrap: [AppComponent]
})
export class AppModule { }
