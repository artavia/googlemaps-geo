import { Component, OnInit, AfterViewInit, ElementRef, Inject, ViewChild, ChangeDetectorRef, AfterContentInit } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { CustomMapComponent } from '../custom-map/custom-map.component';

import { CustomOneshotComponent } from '../custom-oneshot/custom-oneshot.component';
import { CustomContinuousComponent } from '../custom-continuous/custom-continuous.component';

import { GoogleApiKeyService } from '../services/google-api-key.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.scss']
})
export class ContainerComponent implements OnInit, AfterViewInit, AfterContentInit {

  @ViewChild( CustomMapComponent ) mapchild: CustomMapComponent;
  @ViewChild( CustomOneshotComponent ) oneshotchild: CustomOneshotComponent;
  @ViewChild( CustomContinuousComponent ) continuouschild: CustomContinuousComponent;

  // [disabled]="isTracking"
  isTracking: boolean;

  constructor( private svc: GoogleApiKeyService, @Inject( DOCUMENT ) private document, private elRef: ElementRef, private cd: ChangeDetectorRef ) { }

  ngOnInit() { 
  }

  ngAfterViewInit(){
    this.createScriptTag();
    this.mapchild.initializeFirstMap();

    this.isTracking = false;
    this.cd.detectChanges();
  }

  ngAfterContentInit(){ 

    if( ( typeof(this.oneshotchild.timestamp) === 'number' ) && ( typeof(this.continuouschild.timestamp) === 'number' ) ){

      if( this.oneshotchild.timestamp > this.continuouschild.timestamp ){
        this.testOneShotCoords( this.oneshotchild.currentLat, this.oneshotchild.currentLong);
      }
      else
      if( this.oneshotchild.timestamp < this.continuouschild.timestamp ){
        this.testContinuousCoords( this.continuouschild.currentLat, this.continuouschild.currentLong);
      }
      
    }
    else
    if( ( typeof(this.oneshotchild.timestamp) === 'number' ) && ( this.continuouschild.timestamp === undefined ) ){
      this.testOneShotCoords( this.oneshotchild.currentLat, this.oneshotchild.currentLong);
    }
    else
    if( ( this.oneshotchild.timestamp === undefined ) && ( typeof(this.continuouschild.timestamp) === 'number' ) ){
      this.testContinuousCoords( this.continuouschild.currentLat, this.continuouschild.currentLong);
    }
    else{
      // console.log( 'so far its a bust!' );
    } 
  }

  createScriptTag(){
    let baseUrl = `https://maps.googleapis.com`;
    let path = `/maps/api/js`;
    let keyId = this.svc.apiKey;
    let params = `?key=${keyId}`;
    let urlAddress = `${baseUrl}${path}${params}`;
    let s = this.document.createElement( 'script' );
    s.type = "text/javascript";
    s.src = urlAddress; 
    this.elRef.nativeElement.appendChild( s );
  } 

  testOneShotCoords( currentLat, currentLong ){
    let subsequentTitle = 'This is your getCurrentPosition marker.';
    this.mapchild.loadMap( currentLat, currentLong, subsequentTitle );
    // this.oneshotchild.diagnosticMessage = 'Refresh and try again if you made your choice and saw no results.';
  }

  testContinuousCoords( currentLat, currentLong ){
    let subsequentTitle = 'This is your watchPosition marker.';
    this.mapchild.loadMap( currentLat, currentLong, subsequentTitle );
    // this.continuouschild.diagnosticMessage = 'Refresh and try again if you made your choice and saw no results.';
  }

  posGetCurrent(e: Event){
    e.preventDefault();
    this.oneshotchild.geoOneShotObservable(e);
    setTimeout( () => { this.ngAfterContentInit(); }, 3515 ); // 2812 + 703
  }

  posWatch(e: Event){
    e.preventDefault();
    this.continuouschild.geoContinuousObservable(e);
    setTimeout( () => { this.ngAfterContentInit(); }, 3515 ); // 2812 + 703
    this.isTracking = true;
    this.cd.detectChanges();
  }

  stopTrack(e: Event){
    e.preventDefault();
    this.continuouschild.stopWatchObservable(e);
    this.isTracking = false;
    this.cd.detectChanges();
  }

  cullSignal(e: Event){
    e.preventDefault();
  }

}