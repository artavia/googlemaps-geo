// import { Component, OnInit } from '@angular/core';

import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import {} from '@types/googlemaps';

@Component({
  selector: 'app-custom-map',
  templateUrl: './custom-map.component.html',
  styleUrls: ['./custom-map.component.scss']
})
export class CustomMapComponent implements OnInit, AfterViewInit {
  
  @ViewChild('myMap') gMap: ElementRef;
  public map: google.maps.Map;
  public marker: google.maps.Marker;
  
  constructor(  ) { }

  ngOnInit() { 
    // this.initializeFirstMap();
  }

  ngAfterViewInit(){ 
  }
  
  initializeFirstMap(){
    let customlatitude = 10;
    let customlongitude = -84.12;
    let customtitle = 'Initial marker has been set.';
    setTimeout( () => { this.loadMap( customlatitude, customlongitude, customtitle ); }, 2812 ); 
  }

  loadMap( lelat, lelong, letitle ){

    let location = new google.maps.LatLng( lelat, lelong, true );
    const initialLocObj = { lat: lelat , lng: lelong };
    var options = {
      center: location
      , zoom: 15
      , mapTypeId: google.maps.MapTypeId.TERRAIN
    };

    this.map = new google.maps.Map(this.gMap.nativeElement, options);
    this.map.panTo( location );
    this.addMarker( this.marker, this.map , initialLocObj, letitle );
  }

  adjustMap( lelat, lelong, letitle ){

    let location = new google.maps.LatLng( lelat, lelong, true );
    this.map.panTo( location );

    const locObj = { lat: lelat , lng: lelong };
    this.addMarker( this.marker, this.map , locObj, letitle );
  }

  addMarker( mkPm, mapPm , locObjPm, titlePm ) {
    if( !mkPm ){
      mkPm = new google.maps.Marker( {
        position: locObjPm
        , map: mapPm
        , title: titlePm
      } );
      // this.marker = mkPm;
    }
    else {
      mkPm.setPosition( locObjPm );
    }
  }

  whoAmI(){
    return `My name is Lucho and I live to serve Christ the Almighty!`;
  }
  
}
