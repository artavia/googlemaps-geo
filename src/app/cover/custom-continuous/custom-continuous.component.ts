import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/observable/throw';

import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { ChildDiagnosticMessageComponent } from '../child-diagnostic-message/child-diagnostic-message.component';

import { CustomMapComponent } from '../custom-map/custom-map.component';


import { GeoobservableService } from '../services/geoobservable.service';

@Component({
  selector: 'app-custom-continuous',
  templateUrl: './custom-continuous.component.html',
  styleUrls: ['./custom-continuous.component.scss']
})
export class CustomContinuousComponent implements OnInit, AfterViewInit { 
  @ViewChild( ChildDiagnosticMessageComponent) childMessage: ChildDiagnosticMessageComponent;
  
  currentLat: any;
  currentLong: any;
  timestamp: number;
  diagnosticMessage: string;
  
  continuousObservable$: Observable<{}>;
  continuousSubscription: Subscription;
  boundContinuousPosition: any;
  
  // isTracking: boolean = false;

  constructor( public geosvc: GeoobservableService, private cd: ChangeDetectorRef ) { }

  ngOnInit() { }

  ngAfterViewInit(){
    this.childMessage.diagnosticMessage = ""; 
    this.cd.detectChanges();
  }

  geoContinuousObservable(e: Event){
    e.preventDefault();
    this.returnWatchPosition();
  }

  returnWatchPosition() {
    this.continuousObservable$ = this.geosvc.watchLocationObservable;

    const fromObs$ = from( this.continuousObservable$ );
    this.continuousSubscription = fromObs$.subscribe( 
      
      (pos) => { 
        // console.log( 'pos' , pos ); 
        this.childMessage.diagnosticMessage = '';
        this.cd.detectChanges();
        const diagnosticMessage = this.childMessage.diagnosticMessage;

        this.boundContinuousPosition = pos;
        this.currentLat = this.boundContinuousPosition.coords.latitude;
        this.currentLong = this.boundContinuousPosition.coords.longitude;
        this.timestamp = this.boundContinuousPosition.timestamp;
        const currentLat = this.currentLat;
        const currentLong = this.currentLong;
        const timestamp = this.timestamp;

      } 
      , (err) => {
        // console.warn(`ERROR: ${err} `); 
        this.childMessage.diagnosticMessage = `ERROR: ${ err } `;
        this.cd.detectChanges();
        const diagnosticMessage = this.childMessage.diagnosticMessage;
        return Observable.throw(err);
      } 
      , () => {
        // console.log( 'Your query has been completed' ); 
        this.childMessage.diagnosticMessage = 'Your query has been completed';
        this.cd.detectChanges();
        const diagnosticMessage = this.childMessage.diagnosticMessage;
      } 

    );
  }

  stopWatchObservable(e: Event){
    e.preventDefault();
    this.clearWatchPosObservable( this.geosvc.watchId );
  }
  
  clearWatchPosObservable( watchId ){
    // console.log( 'this.geosvc.watchId' , this.geosvc.watchId );
    if( watchId >= 0 ){
      this.childMessage.diagnosticMessage = "geo tracking has been cleared and reset";
      this.cd.detectChanges();
      if( this.geosvc.watchId > 0 ) {
        this.geosvc.watchId = 0;
        navigator.geolocation.clearWatch( watchId );
        // console.log( 'this.geosvc.watchId' , this.geosvc.watchId );
      }
    }
  }

}
