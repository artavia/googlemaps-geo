import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { from } from 'rxjs/observable/from';
import 'rxjs/add/observable/throw';

import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { ChildDiagnosticMessageComponent } from '../child-diagnostic-message/child-diagnostic-message.component';

import { GeoobservableService } from '../services/geoobservable.service';

@Component({
  selector: 'app-custom-oneshot',
  templateUrl: './custom-oneshot.component.html',
  styleUrls: ['./custom-oneshot.component.scss']
})
export class CustomOneshotComponent implements OnInit, AfterViewInit {
  @ViewChild( ChildDiagnosticMessageComponent ) childMessage: ChildDiagnosticMessageComponent; 

  currentLat: any;
  currentLong: any;
  timestamp: number;
  diagnosticMessage: string;

  singleObservable$: Observable<{}>;
  singleSubscription: Subscription;
  boundSinglePosition: any;
  
  // isTracking: boolean = false;

  constructor( public geosvc: GeoobservableService, private cd: ChangeDetectorRef ) { }

  ngOnInit() { }

  ngAfterViewInit(){
    this.childMessage.diagnosticMessage = "";
    this.cd.detectChanges();
  }

  geoOneShotObservable(e: Event){
    e.preventDefault();
    this.returnGetCurrentPosition();
  }

  returnGetCurrentPosition() {
    this.singleObservable$ = this.geosvc.locationObservable;

    const fromObs$ = from( this.singleObservable$ );
    this.singleSubscription = fromObs$.subscribe( 
      
      (pos) => { 
        // console.log( 'pos' , pos ); 
        this.childMessage.diagnosticMessage = '';
        this.cd.detectChanges();
        const diagnosticMessage = this.childMessage.diagnosticMessage;

        this.boundSinglePosition = pos;
        this.currentLat = this.boundSinglePosition.coords.latitude;
        this.currentLong = this.boundSinglePosition.coords.longitude;
        this.timestamp = this.boundSinglePosition.timestamp;
        const currentLat = this.currentLat;
        const currentLong = this.currentLong;
        const timestamp = this.timestamp;

      } 
      , (err) => {
        // console.warn(`ERROR: ${err} `); 
        this.childMessage.diagnosticMessage = `ERROR: ${ err } `;
        this.cd.detectChanges();
        const diagnosticMessage = this.childMessage.diagnosticMessage;
        return Observable.throw(err);
      } 
      , () => {
        // console.log( 'Your query has been completed' ); 
        this.childMessage.diagnosticMessage = 'Your query has been completed';
        this.cd.detectChanges();
        const diagnosticMessage = this.childMessage.diagnosticMessage;
      } 

    );
  }

}
