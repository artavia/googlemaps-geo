import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-diagnostic-message',
  templateUrl: './child-diagnostic-message.component.html',
  styleUrls: ['./child-diagnostic-message.component.scss']
})
export class ChildDiagnosticMessageComponent implements OnInit {

  diagnosticMessage: string;

  constructor() { }

  ngOnInit() {
    this.diagnosticMessage = "";
  }

}
