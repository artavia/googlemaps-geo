import { Observable } from 'rxjs/Observable';

import { Injectable } from '@angular/core';

@Injectable()
export class GeoobservableService {
  
  watchId: number = 0

  constructor() {}
  
  // experiment 1 - getCurrentPosition
  locationObservable = new Observable( (observer) => {
    
    if (navigator.geolocation){
      navigator.geolocation.getCurrentPosition( 
        (pos) => {
          observer.next(pos);
          observer.complete();
          return observer.unsubscribe();
        }
        , () => {
          // console.log( 'Position is not available' );
          observer.error( 'Position is not available' );
        }
        , { enableHighAccuracy: true }
      );
    }
    else {
      observer.error( 'geolocation not supported' );
    }

  } );

  // experiment 2 - watchPosition
  watchLocationObservable = new Observable( (observer) => {
    
    let watchId;

    if (navigator.geolocation){
      
      watchId = navigator.geolocation.watchPosition( 
        (pos) => {
          observer.next(pos);
          // console.log( 'watchId' , watchId );
          observer.complete();
        }
        , () => {
          // console.log( 'Position is not available' );
          observer.error( 'Position is not available' );
        }
        , { enableHighAccuracy: true }
      );

      this.watchId = watchId; 
      // console.log( 'this.watchId' , this.watchId );

    }
    else {
      observer.error( 'geolocation not supported' );
    }

  } );

}
